<?php

/**
 * @file
 * State Flow Collection implementation of the State Machine class.
 */

class StateFlowCollection extends StateFlowEntity {

  public function on_enter_published() {
    // Publish collection and its contents.
    dsm('enter_published');
    dsm($this);
  }

  public function on_enter_unpublished() {
    // Unpublish collection and its contents.
    dsm('enter_unpublished');
    dsm($this);
  }

  public function on_exit_published() {
    // Do something when collection is leaving the published state.
    dsm('exit_published');
    dsm($this);
  }

}
